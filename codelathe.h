#ifndef CODELATHE_H
#define CODELATHE_H

#include <iostream>
#include <fstream>
#include <thread>
#include <zmq.hpp>

#define QUEUE_PUB "tcp://*:1234"
#define QUEUE_SUB "tcp://localhost:1234"

/**
 * @brief The CodeLathe class Create worker thread that creates files received
 * from IPC queue. Main thread jumps into infinite loop and waits for user to
 * input contents of file and desired path
 *
 * This class uses ZMQ for IPC - it covers process synchronization and very easy
 * future scalling to many machines, as well as migration to other
 * architectures It is thread safe, no semaphores or mutexes are needed
 */
class CodeLathe
{
    std::thread worker;
    void worker_thread();

    zmq::context_t context;
    zmq::socket_t socket;

    std::vector<std::pair<std::string, std::string> > recent_inputs;

public:
    CodeLathe();

    /**
     * @brief run Call this function to jump into infinite loop and constantly
     * ask user for pairs: file contents and path. As requirements were to split
     * it by space, file contents should not contain spaces, new lines etc.
     * @return Program exit code
     */
    int run();
};

#endif // CODELATHE_H
