#include "codelathe.h"

using namespace std;

CodeLathe::CodeLathe()
    : socket(context, zmq::socket_type::pub)
{

}

int CodeLathe::run() {
    string cmd, filename;

    // Connect to socket
    socket.bind(QUEUE_PUB);

    // Then create thread, which will connect to our socket
    worker = thread(&CodeLathe::worker_thread, this);

    while (true) {
        cout << "Enter space separated [string] [filename] or [quit] to finish: ";
        cin >> cmd;

        if (cmd == "quit") {
            // This should work also over network, after changing transport from
            // inproc to tcp or other supported by ZMQ
            string quit_msg = "quit";
            socket.send(quit_msg.c_str(), quit_msg.size());
            worker.join();
            break;
        } else if (cmd == "history") {
            cout << "Contents\tPath" << endl;

            for (pair<string, string> entry : recent_inputs) {
                cout << entry.first << "\t" << entry.second << endl;
            }
        } else {
            cin >> filename;

            recent_inputs.push_back(pair<string, string>(cmd, filename));

            string to_send = cmd + " " + filename;
            socket.send(to_send.c_str(), to_send.length());
        }
    }

    socket.close();

    return 0;
}

void CodeLathe::worker_thread() {
    zmq::context_t pull_context;
    zmq::socket_t pull_sock(pull_context, zmq::socket_type::sub);
    pull_sock.connect(QUEUE_SUB);

    // Subscribe to all topics (in ZMQ it means that we don't have any prefix)
    // Adding any prefix to ZMQ will break split to contents/path
    pull_sock.setsockopt(ZMQ_SUBSCRIBE, "", 0);

    while (true) {
        zmq::message_t msg;
        pull_sock.recv(&msg);
        string data((char *)msg.data(), msg.size());

        if (data == "quit") {
            break;
        } else {
            if (data.find(" ") == string::npos) {
                // Malformed data
                continue;
            }

            string contents = data.substr(0, data.find(" "));
            string path = data.substr(data.find(" ")+1, string::npos);

            ofstream file(path, ofstream::out);
            file << contents;
            file.close();
        }
    }

    pull_sock.close();
}
