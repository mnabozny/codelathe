Compilation
===========

Install ZeroMQ dependency and compilation tools:

```
sudo apt install libzmq3-dev cmake make g++
```
or similar commands for non-debian based systems.

Next, compile application:
```
cmake .
make
```

Usage
=====

Launch application without any parameters: ./cl

Then for each line put file contents (no spaces, new lines etc.) and file path, like:

```
some_very_interesting_contents /tmp/my_file
```

File will be written to /tmp/my_file.

To exit application, type: quit
To show recent file edits, type: history

Warning
-------
Application is not checking if file exists and overwrites everything. Use with caution!
